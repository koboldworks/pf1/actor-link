# Changelog

## 4.3.0

- Foundry v12 compatibility

## 4.2.0

- Change: Subskills are now displayed with inherited enhancements on familiar sheets.
- Fix: Class skill bonus now respects `pf1.config.classSkillBonus`

## 4.1.0

- PF1v10 compatibility. Minimum required version increased to v10.

## 4.0.1.1

- Fix: `@_linked` was nonfunctional.

## 4.0.1

- Fix: Selecting master as non-GM failed.
- Fix: Drag&dropping actors as non-GM failed.
- Fix: Selecting different master as non-GM failed.

## 4.0.0.5

- Fix: Errors when familiar has a skill the master doesn't.

## 4.0.0.4

- Fix: Poor handling when the link becomes broken (e.g. parent actor is missing) [#10]

## 4.0.0.3

- Fix: Actors using the link would have incorrect data on them on world launch until any modification.

## 4.0.0.2

- Fix: Linking was limited to linked actors only.

## 4.0.0

- Change: Linking is now accessible via sheet settings tab instead of header bar button.
- Change: Rebranded as _Companion Link_ to avoid confusion with Foundry's actor link flag.
- Fix: Save and Skill override for familiars was nonfunctional.

## 3.0.0

- Foundry v10 and PF1 0.82.x compatibility.

## 2.1.0

- Internal: Less swapped for SCSS.
- Internal: Gulp removed.
- Internal: Release bundling with esbuild.
- Fix: Changelog link in manifest.
- Fix: Minor linguistic correction.
- Change: Link dialog clarifications.

## 2.0.1

- Fix: Class skill bonus was not applied to familiar skills when they had no ranks themselves. [#5]
- Fix: Master's base save stacked instead of applied the difference. [#6]

## 2.0.0.1

- Fix: Normal roll behaviour broken, causing other modules to break.

## 2.0.0

- Basic familiar handling with skill and save overrides when master has better.
- Fix deprecation warnings about `game.pf1.entities`

## 1.2.2

- Fix some problems with generating proper actor link. [#4]
- Link data should no longer be stale
  - Link data is now renewed with each getRollData() call instead of only on force refresh.  
        This ensures changes on parent are up-to-date without needing to force update on the child also.
  - Force refreshing of roll data has been removed as it is no longer necessary with above.

## 1.2.1

- Fix actor linking failing transparently when the previous link was not removed.
- Added warnings for some invalid actions that were silently ignored.
- Some styling changes were made to the dialog. End result is the same.
- Foundry 0.7.x support was removed.

## 1.2.0.2

- Fix manifest.json error leading to old version being constantly served.

## 1.2.0.1

- Fix actor link dialog not opening (ReferenceError: actor is not defined).

## 1.2

- Foundry 0.7.x support dropped again.
- General upkeep to stay up with Foundry development.

## 1.1.1

- Fixed: Actor data being incorrect on world load. [#2, #3]

## 1.1.0.1 Foundry 0.7.x cross-compatibility

## 1.1 Foundry VTT 0.8.x compatibility

## 1.0.1.5 Fix for migration

- Fixed: Migration code had some severe errors. [#1]
- Fixed: Migration was happening every time GM logged in.

## 1.0.1.4 Improved data migration

## 1.0.1.3 Hotfix for link dialog being noisy

## 1.0.1.2 Hotfix for getFlag

## 1.0.1.1 Fix for migration code

## 1.0.1

### Fixed

- Token resource display was bad for other players when linked resources were used to construct them.

## 1.0.0 Dedicated module

### Fixed

- Linked data is sometimes wrong after startup.  
  The method is very hackish and hopefully gains some additional support from PF1 to make it work even more reliably.
