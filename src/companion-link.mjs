import { CFG, getLink, actorName } from './common.mjs';
import { CompanionLinkUI } from './dialog.mjs';

import './overrides.mjs';
import './familiar.mjs';

/**
 * Logic
 *
 * @param actor
 */
function showCompanionLinkUI(actor) {
	const activeWindow = Object.values(ui.windows).find(k => k instanceof CompanionLinkUI && k.object === actor);
	activeWindow?.bringToTop() ?? CompanionLinkUI.open(actor);
}

const templatePath = `modules/${CFG.module}/template/settings-tab.hbs`;
let settingsTemplate;
Hooks.once('setup', () => getTemplate(templatePath).then(t => settingsTemplate = t));

/**
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param {object} options
 */
const injectCompanionLinkConfig = (sheet, [html], options) => {
	const actor = sheet.actor;
	if (!['character', 'npc'].includes(actor.type)) return;

	const settings = html.querySelector('form section .tab.settings[data-tab="settings"]'),
		linkData = getLink(actor),
		linkedActor = game.actors.get(linkData?.id);

	const templateData = {
		editable: sheet.isEditable,
		isLinked: !!linkData?.id && !!linkedActor,
		canLink: true, // actor.prototypeToken.actorLink,
		id: linkData?.id,
		actor: linkedActor,
		get name() { return this.actor?.name; },
		get img() {
			return this.actor?.token?.texture.src ?? this.actor?.prototypeToken.texture.src ?? CONST.DEFAULT_TOKEN;
		},
		isFamiliar: linkData?.familiar ?? false,
	};

	const div = document.createElement('div');
	div.innerHTML = settingsTemplate(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });

	div.querySelector('.identity')
		?.addEventListener('contextmenu', ev => {
			ev.preventDefault();
			ev.stopPropagation();

			linkedActor?.sheet?.render(true, { focus: true });
		});

	div.querySelector('button')
		?.addEventListener('click', ev => {
			ev.preventDefault();
			ev.stopPropagation();

			showCompanionLinkUI(actor);
		});

	settings?.append(...div.childNodes);
};

/*
function injectCompanionLinkButton(sheet, buttons) {
	if (!sheet.isEditable) return;

	const actor = sheet.actor;

	buttons.unshift({
		class: 'companion-link-button',
		icon: 'fas fa-link',
		label: game.i18n.localize('Koboldworks.CompanionLink.Title'),
		onclick: _ => showCompanionLinkUI(actor),
	});
}
*/

class CompanionLink {
	// shoddy way to avoid error spam
	static lastWarning = {
		NotFound: null,
		Recursion: null,
	};

	static getRollData(doc, rollData, refresh) {
		// if (!refresh) return; // quick refresh, old link should be valid; Heeding this can cause stale linked data.
		if (!(doc instanceof Actor)) return; // don't do anything with items

		// Clear as precaution
		delete rollData._linked;

		// Get link data
		const linkData = getLink(doc);
		if (linkData?.id === undefined) return; // no link

		// Get linked actor
		const linkedActor = game.actors?.get(linkData.id); // game actors are not ready before PF starts doing stuff with actors
		if (!linkedActor) {
			if (CompanionLink.lastWarning.NotFound !== doc.id) {
				console.warn('LINKED actor not found:', actorName(doc), '->', linkData.id);
				CompanionLink.lastWarning.NotFound = doc.id;
			}
			return;
		}
		if (doc === linkedActor) {
			console.error('Companion Link 🔗 | Linked to self:', actorName(doc));
			return; // shouldn't happen, but here just in case.
		}

		// Make sure there's no link chaining (and potential infinite loops by extension)
		if (getLink(linkedActor)?.id !== undefined) {
			if (CompanionLink.lastWarning.Recursion !== doc.id) {
				console.error(`Companion Link 🔗 | Recursion Guard | ${actorName(doc)} to ${actorName(linkedActor)}.`);
				CompanionLink.lastWarning.Recursion = doc.id;
			}
			return;
		}

		// Lazy _linked roll data fetching
		rollData._linked = linkedActor?.getRollData();
	}

	/**
	 * Workaround for other actors not being available during world load.
	 *
	 * @param actorIds
	 */
	static async forceRefresh(actorIds) {
		if (actorIds.length == 0) return;

		console.log('Companion Link 🔗 | Force Refresh | Actors:', actorIds.length);
		if (game.user.isGM) console.warn('Companion Link 🔗 | Force Refresh | Actor IDs:', actorIds);

		for (const actorId of actorIds) {
			const actor = game.actors.get(actorId);

			try {
				if (game.user.isGM) console.log('Companion Link 🔗 | Force Refresh |', actor.name, actor.id);
				actor.reset();
				// actor.getRollData({ refresh: true }); // Force refresh rollData, may be unnecessary
			}
			catch (err) {
				console.error('Companion Link 🔗 | Force Refresh | Error:', err);
			}
		}
	}

	/**
	 * Do final cleanup.
	 */
	static async finalize() {
		await CompanionLink.forceRefresh(CompanionLink.needReprep);

		// Cleanup
		delete CompanionLink.needReprep;
		delete CompanionLink.earlyPreparation;
		delete CompanionLink.forceRefresh;
		delete CompanionLink.finalize;
	}

	static needReprep = [];
	static earlyPreparation(actor) {
		const link = getLink(actor);
		if (link?.id) CompanionLink.needReprep.push(actor.id);
	}
}

Hooks.on('pf1PrepareBaseActorData', CompanionLink.earlyPreparation);

Hooks.once('ready', () => {
	Hooks.off('pf1PrepareBaseActorData', CompanionLink.earlyPreparation);

	// Hooks.on('pf1.getRollData', CompanionLink.getRollData); // 0.81.3 and older
	Hooks.on('pf1GetRollData', CompanionLink.getRollData);

	CompanionLink.finalize().then(() => {
		// Hooks.on('getActorSheetPFHeaderButtons', injectCompanionLinkButton);
		Hooks.on('renderActorSheet', injectCompanionLinkConfig);
		console.log('Companion Link 🔗 |', game.modules.get(CFG.module).version, '| Ready');
	});
});
