export const CFG = {
	module: 'koboldworks-pf1-actor-link',
	SETTINGS: {
		linkKey: 'actorLink',
		keymaster: 'koboldworks',
		defaultLink: '_linked',
	},
	COLORS: {
		main: 'color:mediumseagreen',
		label: 'color:darkseagreen',
		unset: 'color:unset',
	},
};

/**
 * @param actor
 * @returns {string}
 */
export const actorName = (actor) => game.user.isGM ? `${actor.name} [${actor.id}]` : `${actor.id}`;

/**
 * @param {ActorPF} actor
 * @returns {object | undefined}
 */
export function getLink(actor) {
	const flag = actor.getFlag(CFG.module, CFG.SETTINGS.linkKey);
	if (flag) return foundry.utils.deepClone(flag);
}

export const setLink = async (actor, linkData) => actor.setFlag(CFG.module, CFG.SETTINGS.linkKey, linkData);

export const delLink = async (actor) => actor.unsetFlag(CFG.module, CFG.SETTINGS.linkKey);

const formatter = new Intl.NumberFormat('nu', { signDisplay: 'always' });
export const signNum = (v) => formatter.format(v);

export function getClassBaseSave(cls, key) {
	const sysData = cls.system;
	const clsRollData = {
		level: sysData.level,
		hitDice: cls.hitDice,
	};
	const saveType = sysData.savingThrows[key].value;
	let formula = saveType === 'custom' ? sysData.savingThrows[key].custom || '0' : CONFIG.PF1.classSavingThrowFormulas[cls.subType][saveType];
	if (formula == null) formula = '0';

	return Roll.defaultImplementation.safeRollSync(formula, clsRollData).total;
}
