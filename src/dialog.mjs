import { getLink, setLink, delLink, CFG } from './common.mjs';

export class CompanionLinkUI extends FormApplication {
	linked = undefined;
	linkedId = undefined;
	linkData = undefined;
	familiar = false;
	actor;

	constructor(actor, options) {
		super(actor, options);
		this.actor = actor;

		this.options.title = game.i18n.format('Koboldworks.CompanionLink.TitleBar', { name: this.actor.name });
		this.options.classes.push(`companion-link-id-${this.actor.id}`);
		this.linkedId = undefined;
		this.linked = undefined;
		this.familiar = false;
	}

	get template() {
		return `modules/${CFG.module}/template/dialog.hbs`;
	}

	static open(actor) {
		new CompanionLinkUI(actor).render(true, { focus: true });
	}

	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			classes: [...options.classes, 'koboldworks', 'koboldworks-companion-link'],
			renderUpdateKeys: ['linkedId', 'linked', 'linkedImg'],
			width: 480,
			dragDrop: [{ dropSelector: null }],
			closeOnSubmit: true,
		};
	}

	async getData(options = {}) {
		const data = super.getData(options);

		data.isGM = game.user.isGM;

		this.linkData = getLink(this.actor);

		data.actor = this.actor;
		data.linkedId = this.linkedId = this.linkData?.id;
		data.linked = this.linked = game.actors.get(data.linkedId);
		data.linkedImg = this.linkedImg = this.linked?.img ?? this.linked?.token?.img ?? CONST.DEFAULT_TOKEN;
		data.familiar = this.familiar = this.linkData?.familiar ?? this.familiar;

		// data.validActorChoices = game.user.isGM ? [] : [null, ...game.actors.filter(o => o.owner)];
		const actorChoices = { '': '' };

		if (!data.isGM) {
			game.actors
				.filter(actor => actor.isOwner && actor.id !== this.actor.id)
				.forEach(actor => actorChoices[actor.uuid] = `${actor.name} [${actor.id}]`);
		}

		data.validActorChoices = actorChoices;

		return data;
	}

	/**
	 * @param {string} uuid Actor UUID
	 */
	async _linkActor(uuid) {
		const linkedActor = fromUuidSync(uuid);

		const actorId = linkedActor.id;
		if (this.actor.id === actorId) {
			const msg = 'Linking to self is not allowed.';
			ui.notifications.warn(msg);
			console.warn(msg);
			return; // Don't allow linking to self.
		}

		if (linkedActor) {
			this.linkData = { id: actorId, familiar: this.familiar };
			this.linkedId = actorId;
			// HACK: invalidate _linked rolldata
			const rollData = this.actor?._rollData ?? {};
			delete rollData._linked;

			await setLink(this.actor, this.linkData);
			// this.actor.getRollData({ refresh: true });
			this.linked = linkedActor;
		}
		else {
			console.error('Companion Link 🔗 | Actor', actorId, 'not found!');
			ui.notifications.warn('Linked actor could not be found.');
		}

		return this.render(false);
	}

	async _onUnlink(_event) {
		if (this.linkedId !== null) {
			await delLink(this.actor);
			this.linked = undefined;
			this.linkedId = undefined;
			this.familiar = false;

			return this.render(true);
		}
	}

	/**
	 * @override
	 */
	_canDragDrop() {
		return this.isEditable;
	}

	async _onDrop(event) {
		if (!this.isEditable) return;

		const data = TextEditor.getDragEventData(event);
		if (data.type === 'Actor')
			this._linkActor(data.uuid);
	}

	_openLinked(_event) {
		const sheet = this.linked?.sheet;
		if (sheet) {
			if (sheet.rendered) {
				sheet.maximize();
				sheet.bringToTop();
			}
			else sheet.render(true);
		}
	}

	_onSelectChange(event) {
		const newUuid = event.target.value;
		if (newUuid)
			this._linkActor(newUuid);
		else
			this._onUnlink(event);
	}

	async _onFamiliarToggle(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		const familiar = ev.target.checked;
		this.familiar = familiar;
		if (this.linkData) {
			this.linkData.familiar = familiar;
			console.log('Companion Link 🔗 | Saving link data:', this.linkData);
			await setLink(this.actor, this.linkData)
				.then(_ => this.render());

			const link = getLink(this.actor);
		}
	}

	activateListeners(html) {
		super.activateListeners(html);

		html.find('.companion-unlink').on('click', this._onUnlink.bind(this));
		html.find('.companion-link-img').on('click', this._openLinked.bind(this));
		html.find('select[name=actor]').on('change', this._onSelectChange.bind(this));
		html.find('input[name="familiar"]').on('change', this._onFamiliarToggle.bind(this));
	}
}
