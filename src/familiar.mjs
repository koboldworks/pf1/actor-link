/**
 * Familiar handling
 */

import { getLink, signNum, getClassBaseSave } from './common.mjs';

/**
 * @param {Actor} actor
 */
const getBaseSaves = (actor) => {
	return actor.items
		.filter(i => i.type === 'class')
		.reduce((saves, cls) => {
			['fort', 'ref', 'will'].forEach(key => saves[key] += getClassBaseSave(cls, key));
			return saves;
		}, { ref: 0, fort: 0, will: 0 });
};

/**
 * Add master's skills and saves.
 *
 * @param {ActorSheet} sheet
 * @param {JQuery} html
 * @param _data
 */
const enhanceFamiliar = (sheet, [html], _data) => {
	const familiar = sheet.actor;
	const linkData = getLink(familiar);
	if (!linkData) return;

	const master = game.actors.get(linkData.id);
	if (!master || !linkData.familiar) return;

	// Get all skills
	html.querySelector('.tab.skills[data-tab="skills"]')
		?.querySelectorAll('.skill[data-skill]')
		?.forEach(el => {
			const skill = el.dataset.skill;
			const subskill = el.dataset.subSkill;

			const skillId = subskill ? `${skill}.${subskill}` : skill;

			let mSkInfo;
			try {
				mSkInfo = master.getSkillInfo(skillId);
			}
			catch (err) {
				// ignore, master doesn't have the skill
				return;
			}

			if (!(mSkInfo.rank > 0)) return; // Master doesn't have the skill

			const fSkInfo = familiar.getSkillInfo(skillId);
			const diff = mSkInfo.rank - fSkInfo.rank;
			if (diff <= 0) return; // Same rank or better, ignore

			const mod = el.querySelector('.skill-mod');
			if (!mod) return;

			mod.classList.add('koboldworks', 'familiar', 'master-enhanced');

			const csBonus = (fSkInfo.cs && fSkInfo.rank == 0) ? pf1.config.classSkillBonus : 0;
			const skillMod = fSkInfo.mod + diff + csBonus;
			mod.textContent = `${signNum(skillMod)}`;
		});

	const mSaves = getBaseSaves(master),
		fSaves = getBaseSaves(familiar);

	// Ugly selector for the saves
	html.querySelector('.tab.summary .quick-info')
		?.querySelectorAll('.saving-throw').forEach(el => {
			const save = el.dataset.savingthrow,
				ms = mSaves[save], fs = fSaves[save],
				diff = ms - fs;

			if (diff > 0) {
				el.classList.add('koboldworks', 'save-mod', 'familiar', 'master-enhanced');
				const ntotal = familiar.system.attributes.savingThrows[save].total + diff;
				const value = el.querySelector('.value');
				value.textContent = `${signNum(ntotal)}`;
			}
		});
};

function enchanceFamiliarTooltip(app, id, template) {
	const parts = id.split('.');
	id = parts.shift();
	if (!['save', 'skill'].includes(id)) return;

	const familiar = app.actor;
	const linkData = getLink(familiar);
	if (!linkData) return;

	const master = game.actors.get(linkData.id);
	if (!master || !linkData.familiar) return;

	const d = document.createElement('template');
	switch (id) {
		case 'save': {
			const save = parts.pop(),
				mSaves = getBaseSaves(master),
				fSaves = getBaseSaves(familiar);

			const ms = mSaves[save],
				fs = fSaves[save],
				diff = ms - fs;

			if (diff <= 0) return;

			// Mimic PF1v10 styling
			d.innerHTML = `<h4 class="companion-link">Companion Link</h4><span class='detail'>Master Base Save:</span><span class='value'>${ms}</span><span class='detail'>Familiar Base Save:</span><span class='value'>${fs}</span><span class='detail'>Difference:</span><span class='value'>${signNum(diff)}</span>`;
			break;
		}
		case 'skill': {
			const skillId = parts.join('.');
			let mSkInfo;
			try {
				mSkInfo = master.getSkillInfo(skillId);
			}
			catch (err) {
				// ignore, master doesn't have the skill
				return;
			}
			if (!(mSkInfo?.rank > 0)) return; // Master doesn't have the skill

			const fSkInfo = familiar.getSkillInfo(skillId);
			const diff = mSkInfo.rank - fSkInfo.rank;
			if (diff <= 0) return; // Same rank or better, ignore

			const tipp = [['Master ranks:', mSkInfo.rank], ['Familiar ranks:', fSkInfo.rank], ['Difference:', signNum(diff)]];
			if (fSkInfo.cs && fSkInfo.rank == 0)
				tipp.push(['Class skill bonus:', signNum(3)]);
			const h = document.createElement('h4');
			h.textContent = 'Companion Link';
			d.content.append(h);
			for (const [label, value] of tipp) {
				const l = document.createElement('span');
				l.classList.add('detail');
				l.textContent = label;
				const v = document.createElement('span');
				v.classList.add('value');
				v.textContent = value;
				d.content.append(l, v);
			}
			break;
		}
	}
	template.content.append(d.content);
}

Hooks.on('renderActorSheetPF', enhanceFamiliar);
Hooks.on('renderPF1ExtendedTooltip', enchanceFamiliarTooltip);
