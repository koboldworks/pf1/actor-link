import { CFG, getLink, signNum, getClassBaseSave } from './common.mjs';

// Rules reference: <https://www.aonprd.com/ClassDisplay.aspx?ItemName=Familiar>

/**
 * @param {string} save Save ID
 * @param {Actor} familiar
 * @param {Actor} master
 * @param {object} data
 */
function overrideSave(save, familiar, master, data) {
	const mClasses = master.items.filter(i => i.type === 'class'),
		mBaseSave = mClasses.reduce((value, cls) => value + getClassBaseSave(cls, save), 0),
		newParts = [...data.parts],
		fClasses = familiar.items.filter(i => i.type === 'class'),
		famBase = fClasses.reduce((value, cls) => value + getClassBaseSave(cls, save), 0),
		diff = mBaseSave - famBase;

	if (diff <= 0) return; // Master has worse base, ignore
	if (famBase !== 0) data.parts = newParts;

	data.parts.unshift(`${diff}[Master's Base Save Bonus]`);

	if (!data.chatTemplateData.hasProperties) data.chatTemplateData.properties = [];
	data.chatTemplateData.properties.push(`Master Base: ${signNum(diff)}`);
	data.chatTemplateData.hasProperties = true;
}

function overrideSkill(skill, familiar, master, data) {
	const mSkInfo = master.getSkillInfo(skill);
	if (!(mSkInfo?.rank > 0)) return; // Master doesn't have the skill

	const fSkInfo = familiar.getSkillInfo(skill),
		mSkRank = mSkInfo.rank,
		diff = mSkRank - fSkInfo.rank;
	if (diff <= 0) return; // Same rank or better, ignore

	// Replace skill ranks
	const i = data.parts.findIndex(p => p.includes(`[${game.i18n.localize('PF1.SkillRankPlural')}]`));
	const newRankPart = `${mSkRank}[${game.i18n.localize('PF1.SkillRankPlural')}; Master]`;
	if (i >= 0) data.parts.splice(i, 1, newRankPart);
	else data.parts.unshift(newRankPart);

	// Add missing class skill bonus
	if (fSkInfo.rank == 0 && fSkInfo.cs)
		data.parts.push(`3[${game.i18n.localize('PF1.CSTooltip')}]`);

	if (!data.chatTemplateData.hasProperties) data.chatTemplateData.properties = [];
	data.chatTemplateData.properties.push(`Master Ranks: ${signNum(diff)}`);
	data.chatTemplateData.hasProperties = true;
}

function d20RollOverride(wrapped, data) {
	// Preroll
	const skill = data.subject?.skill,
		save = data.subject?.save;

	if (skill || save) {
		const familiar = ChatMessage.getSpeakerActor(data.speaker);
		const link = getLink(familiar);
		const master = link?.familiar ? game.actors.get(link.id) : null;
		if (master) {
			if (skill) overrideSkill(skill, familiar, master, data);
			else if (save) overrideSave(save, familiar, master, data);
		}
	}

	return wrapped.call(this, data);
}

Hooks.once('pf1PostInit', function enableFamiliarOverrides() {
	/* global libWrapper */
	libWrapper.register(CFG.module, 'pf1.dice.d20Roll', d20RollOverride, 'WRAPPER');
});
