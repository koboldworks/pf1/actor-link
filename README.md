# Koboldworks – Companion Link for Pathfinder 1e

Link one character to another to gain access to their roll data.

## Details

### Example

#### Familiar

1) On the familiar, open link UI in the settings tab.
2) Select the master as the link partner.
3) Tick familiar checkbox.
4) Add a Change to some familiar feature.
5) Target HP with `floor(@_linked.attributes.hp.max / 2)` as the formula.

If HP is wrong after this, adjust priority of the HP Change.

The HP change part is not automated for now, since there are familiars that get full HP or quarter HP instead of the default half.

### Usage

Configuration is reached via actor sheet settings tab.

The interface supports only drag & drop. There's very little checking done if the actor is meaningful, just that it's an actor.  
![Link Setup](./img/screencaps/setup.png)

Refer to linked data via `@_linked` variable in formulas.

![Link Usage](./img/screencaps/usage.png)

This is also accessible via `actor.getRollData()._linked` for macros as you'd expect, nothing unusual is done with that.

### Rationale

Familiars, Animal Companions, Eidolons, Phantoms, etc. benefit from having a link to their owner. This is here to provide a minimal solution to it.

Familiars have more automation included by toggling the familiar checkbox in the link dialog (does not include health).

### Configuration

None available.

### Limitations

Infinite loop and similar guards were employed. So no infinite loop creation. This also prevents any chaining of linked actors to any degree beyond one link.

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/actor-link/-/raw/latest/module.json>

### PF1v9 and older

Manual install only:

- Manifest: <https://gitlab.com/koboldworks/pf1/actor-link/-/releases/4.0.1.1/downloads/module.json>
- Zip: <https://gitlab.com/koboldworks/pf1/actor-link/-/releases/4.0.1.1/downloads/companion-link.zip>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
