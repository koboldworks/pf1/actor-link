/**
 * Link cleanup
 *
 * Function:
 * - Removes dead links.
 */

const oldLinkKey = 'actorId',
	newLinkKey = 'actorLink',
	defaultLink = '_linked',
	keymaster = 'koboldworks',
	module = 'koboldworks-pf1-actor-link';

let cleanupAttempts = 0, cleanupDone = 0, skipped = 0;

async function cleanup(actor) {
	cleanupAttempts++;
	if (!actor.isOwner) {
		skipped++;
		return; // redundant
	}

	const olink = foundry.utils.getProperty(actor.data.flags, `${keymaster}.${oldLinkKey}`);
	const nlink = actor.getFlag(module, newLinkKey);

	const updateData = {};

	let dold = false, dnew = false;

	if (olink !== undefined) {
		const oldl = `flags.${keymaster}.-=${oldLinkKey}`;
		updateData[oldl] = null; // delete
		dold = true;
	}

	if (game.actors.get(nlink.id) == undefined) {
		const newl = `flags.${module}.-=${newLinkKey}`;
		updateData[newl] = null;
		dnew = true;
	}

	if (dold || dnew) {
		console.log('Cleaning actor:', actor.name, actor.id, dold ? 'Old Data' : '', dnew ? 'New Data' : '');
		cleanupDone++;
		return actor.update(updateData);
	}
}

if (game.modules.get(module) !== undefined) {
	ui.notifications.info('Cleaning dead link data from all actors.');
	console.group('Actor Data cleanup');
	try {
		if (game.user.isGM)
			game.actors.forEach(cleanup);
		else
			ui.notifications.warn('GM required');
	}
	catch (err) {
		console.error(err);
	}
	finally {
		console.log('Cleanup done!');
		console.group('Statistics');
		console.log('Attempts:', cleanupAttempts);
		console.log('Skipped:', skipped);
		console.log('Cleaned:', cleanupDone);
		console.groupEnd();
		console.groupEnd();
	}
	ui.notifications.info(`Link data cleanup complete (${cleanupDone} updated).`);
}
else {
	ui.notifications.warn('You do not have prerequisite module installed. This script is not meant for you.');
}
