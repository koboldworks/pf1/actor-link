/**
 * Link migration
 *
 * This attempts to fully migrate data from Koboldworks to dedicated module.
 * This also cleans up old dead links.
 */

const oldLinkKey = 'actorId',
	newLinkKey = 'actorLink',
	defaultLink = '_linked',
	keymaster = 'koboldworks',
	module = 'koboldworks-pf1-actor-link';

let skipped = 0, notNeeded = 0, total = 0;

/** @param {ActorPF} actor */
async function migrate(actor) {
	total++;
	if (!actor.isOwner) {
		skipped++;
		return; // redundant
	}

	const link = foundry.utils.getProperty(actor.data.flags, `${keymaster}.${oldLinkKey}`);
	if (link === undefined) {
		notNeeded++;
		return; // no old, skipping
	}

	const updateData = {};

	console.log('Migrating:', actor.name, actor.id);
	if (game.actors.get(link)) {
		// construct new
		const newl = `flags.${module}.${newLinkKey}`;
		const linkData = { id: link };
		updateData[newl] = linkData;
	}
	else {
		console.warn('Target not found:', link, '– Cleaning');
		const oldl = `flags.${keymaster}.-=${oldLinkKey}`;
		updateData[oldl] = null; // delete
	}

	return actor.update(updateData);
}

if (game.modules.get(module) !== undefined) {
	ui.notifications.info('Migrating Companion Link data for all actors.');
	console.group('Companion Link migration');
	try {
		if (game.user.isGM)
			game.actors
				.forEach(migrate);
	}
	catch (err) {
		console.error(err);
	}
	finally {
		console.log('Migration complete');
		console.group('Statistics');
		console.log('Total actors:', total);
		console.log('Skipped:', skipped);
		console.log('No old link:', notNeeded);
		console.groupEnd();
	}
	ui.notifications.info('Companion Link data migration complete.');
}
else {
	ui.notifications.warn('You do not have prerequisite module installed. This script is not meant for you.');
}
